const mongoose = require('mongoose')

const connectDB = async () => {
  try {
    mongoose.set('strictQuery', false)
    mongoose.connect(process.env.MONGO_URI)
  } catch (error) {
    console.log('[DATABASE_ERR]', error)
    process.exit()
  }
}

module.exports = connectDB