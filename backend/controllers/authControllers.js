const { hashPassword, comparePassword } = require('../helpers/auth')
const User = require('../models/userModel')
const jwt = require('jsonwebtoken')

const test = (req, res) => {
  res.json({ msg: "bienvenu dans le backend" })
}

const createUser = async (req, res) => {
  try {
    const { name, email, password } = req.body

    if (!name) {
      return res.json({ error: "Le nom est requis" })
    }
    if (!email) {
      return res.json({ error: "L'adresse email est requise" })
    }
    if (!password || password.length < 8) {
      return res.json({ error: "Le mot de pâsse est requis et doit contenir au moins 8 caractères" })
    }

    const emailExist = await User.findOne({ email })
    if (emailExist) {
      return res.json({ error: "Un compte est déja associé à cette adresse email, si vous avez oublié votre mot de passe, cliquez sur le lien 'mot de passe oublié'" })
    }

    const hashedPassword = await hashPassword(password)

    const newUser = await User.create({ name, email, password: hashedPassword })

    return res.json({ msg: "Votre compte a bien été crée, vous pouvez désormais vous connecter", user: newUser })
  } catch (error) {
    console.log('[CREATE_USER]', error)
  }
}

const loginUser = async (req, res) => {
  try {
    const { email, password } = req.body

    if (!email) {
      return res.json({ error: "L'adresse email est requise" })
    }
    if (!password) {
      return res.json({ error: "Le mot de passe est requis" })
    }

    await User.findOne({ email })
      .then(async (user) => {
        if (!user) {
          return res.json({ error: "Cet utilisateur n'existe pas" })
        }
        await comparePassword(password, user.password)
          .then((match) => {
            if (!match) {
              return res.json({ error: "Mot de passe incorrect" })
            }
            jwt.sign({ _id: user._id, name: user.name, email: user.email }, process.env.JWT_SECRET, { expiresIn: '7d' }, (err, token) => {
              if (err) throw err
              res.cookie('token', token).json({ msg: "Connexion réussie", user })
            })
          })
      })
  } catch (error) {
    console.log('[LOGIN]', error)
  }
}

const getProfile = (req, res) => {
  const { token } = req.cookies

  if (!token) {
    return res.json(null)
  }
  jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
    if (err) throw err
    res.json(user)
  })
}

module.exports = {
  test,
  createUser,
  loginUser,
  getProfile
}