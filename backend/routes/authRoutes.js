const express = require('express')
const router = express.Router()
const cors = require('cors')
const { test, createUser, loginUser, getProfile } = require('../controllers/authControllers')

router.get('/', test)

router.post('/register', createUser)

router.post('/login', loginUser)

router.get('/profile', getProfile)

module.exports = router