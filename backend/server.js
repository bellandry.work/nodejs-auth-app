const express = require('express')
const dotenv = require('dotenv').config()
const cors = require('cors')
const connectDB = require('./config/db')
const port = process.env.PORT || 5000
const cookieParser = require('cookie-parser')

/* Database connexion */
connectDB()

const app = express()

/* Middlewares */
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cors(
  {
    origin: process.env.CLIENT_URL,
    credentials: true
  }
))
app.use(cookieParser())

app.use('/', require('./routes/authRoutes'))

app.listen(port, () => console.log(`Server is running on port ${port}`))