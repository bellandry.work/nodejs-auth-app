import axios from 'axios'
import { createContext, useEffect, useState } from "react"

export const UserContext = createContext({})

export const UserContextProvider = ({ children }) => {
  const [loggedUser, setLoggedUser] = useState(null)

  useEffect(() => {
    if (!loggedUser) {
      axios.get('/profile').then(({ data }) => {
        setLoggedUser(data)
      })
    }
  }, [])

  return (
    <UserContext.Provider value={{ loggedUser, setLoggedUser }}>
      {children}
    </UserContext.Provider>
  )
}