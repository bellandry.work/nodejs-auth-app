import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { UserContext } from '../../context/userContext'

const NavbarMobile = () => {
  const { loggedUser } = useContext(UserContext)

  return (
    <>
      <div className="md:hidden w-2/5 flex flex-col items-end gap-4 p-4 absolute right-4 top-[80px] text-lg text-neutral-900 bg-white shadow-md rounded-md transition-all">
        <Link to='/' className="hover:text-orange-400 font-semibold transition-all">Accueil</Link>
        {loggedUser ? (
          <>
            <Link to='/dashboad' className="hover:text-orange-400 font-semibold transition-all">Dashboard</Link>
            <p className="font-semibold">Bonjour {loggedUser.name}</p>
          </>
        ) : (
          <>
            <Link to='/register' className="hover:text-orange-400 font-semibold transition-all">Inscription</Link>
            <Link to='/login' className="hover:text-orange-400 font-semibold transition-all">Connexion</Link>
          </>
        )}
      </div>
    </>
  )
}

export default NavbarMobile