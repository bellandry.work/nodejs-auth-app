import { useContext, useState } from "react"
import { Link } from "react-router-dom"
import { UserContext } from "../../context/userContext"
import NavbarMobile from "./NavBarMobile"

function Navbar() {
  const { loggedUser } = useContext(UserContext)
  const [menu, setMenu] = useState()

  return (
    <nav className='flex items-center bg-slate-700 text-white w-full h-[70px]'>
      <div className="container flex items-center mx-auto w-full px-8 justify-between">
        <div className="">
          <p className="font-bold text-3xl text-transparent bg-clip-text bg-gradient-to-r from-orange-700 to-yellow-600">Laclass Logo</p>
        </div>
        <div className="md:hidden flex flex-col w-10 gap-2 items-end" onClick={() => setMenu(menu => !menu)}>
          <div className="w-full h-1 bg-neutral-100 rounded-full"></div>
          <div className="w-4/6 h-1 bg-neutral-100 rounded-full"></div>
          <div className="w-2/6 h-1 bg-neutral-100 rounded-full"></div>
        </div>
        {menu && <NavbarMobile />}
        <div className="hidden md:flex items-center justify-end gap-x-8">
          <Link to='/' className="hover:text-orange-400 font-semibold transition-all">Accueil</Link>
          {loggedUser ? (
            <>
              <Link to='/dashboad' className="hover:text-orange-400 font-semibold transition-all">Dashboard</Link>
              <p className="font-semibold">Bonjour {loggedUser.name}</p>
            </>
          ) : (
            <>
              <Link to='/register' className="hover:text-orange-400 font-semibold transition-all">Inscription</Link>
              <Link to='/login' className="hover:text-orange-400 font-semibold transition-all">Connexion</Link>
            </>
          )}
        </div>
      </div>
    </nav>
  )
}

export default Navbar