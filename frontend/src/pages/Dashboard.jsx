import React, { useContext } from 'react'
import { UserContext } from '../../context/userContext'

function Dashboard() {
  const { loggedUser } = useContext(UserContext)

  return (
    <>
      <h1 className='text-4xl font-bold text-neutral-900'>Dashboard</h1>
      <div className='w-full h-full flex items-center justify-center'>
        {loggedUser && <h1 className='text-4xl font-bold'>Bienvenu {loggedUser.name} </h1>}
      </div>
    </>
  )
}

export default Dashboard