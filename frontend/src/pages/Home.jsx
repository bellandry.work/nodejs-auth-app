import React from 'react'
import { Link } from 'react-router-dom'

function Home() {
  return (
    <div className='w-full h-[calc(100vh-70px)] flex items-center justify-center bg-neutral-200'>
      <div className='flex flex-col gap-12 '>
        <h1 className='text-5xl md:text-6xl font-bold text-center'>Bienvenu sur NodeAuth</h1>
        <p className='text-lg text-center'>Une simple application pour apprendre les bases de l'authentification avec nodejs</p>
        <Link
          to='/register'
          className='flex items-center justify-center py-8'
        >
          <span className='bg-slate-700 ring-1 text-white font-bold py-3 px-6 md:px-8 rounded-md w-fit hover:bg-transparent hover:text-slate-900 hover:ring-slate-800 transition-all'>
            Commencer
          </span>
        </Link>
      </div>
    </div>
  )
}

export default Home