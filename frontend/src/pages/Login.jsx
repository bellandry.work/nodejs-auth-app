import axios from 'axios'
import React, { useState } from 'react'
import toast from 'react-hot-toast'
import { Link, useNavigate } from 'react-router-dom'

function Login() {
  const navigation = useNavigate()

  const [user, setUser] = useState({
    email: '',
    password: ''
  })

  const loginUser = async (e) => {
    e.preventDefault()
    try {
      const { data } = await axios.post('/login', user)

      if (data.error) {
        toast.error(data.error)
      } else {
        setUser({})
        toast.success(data.msg)
        navigation('/dashboard')
      }
    } catch (error) {
      console.log('catch error : ', error.message)
    }
  }

  return (
    <div className='flex w-full md:w-1/3 mx-auto h-[calc(100vh-100px)] justify-center'>
      <form onSubmit={loginUser} className="flex flex-col gap-2 font-semibold justify-center h-full w-full">
        <h1 className="text-4xl font-bold">Connectez vous</h1>
        <br />
        <label htmlFor="email" className="w-full">Votre email</label>
        <input type="email" name="email" id="email" className="w-full ring-1 ring-slate-400 rounded-sm p-4" value={user.email} onChange={(e) => setUser({ ...user, email: e.target.value })} />
        <label htmlFor="password" className="w-full">Votre nome</label>
        <input type="password" name="password" id="password" className="w-full ring-1 ring-slate-400 rounded-sm p-4" value={user.password} onChange={(e) => setUser({ ...user, password: e.target.value })} />
        <br />
        <button type="submit" className="text-white bg-slate-700 py-4 rounded-md hover:bg-transparent hover:text-slate-700 hover:border-slate-700 ring-1 transition-all">Connexion</button>
        <br />
        <p className='font-normal'>Pas encore de compte ? <Link to='/login' className='font-semibold underline'>Inscrivez vous</Link></p>
      </form>
    </div>
  )
}

export default Login